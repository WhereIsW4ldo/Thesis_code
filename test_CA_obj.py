#!/usr/bin/python3.11
# -*- coding: utf-8 -*-

print("this file is not up to date!")
exit(0)

from FOLACQ import Constraint_Acquisition as ConsAcq

import argparse

parser = argparse.ArgumentParser(
    prog = "python test_CA_obj.py",
    description = "This program takes in a certain problem (as examples) and determines the rules within that problem",
    epilog = "This code is still in development and thus can still contain bugs"
)

parser.add_argument('-v', '--verbose', \
                    action='store_true')
parser.add_argument('-i', '--iterations', \
                    type=int, default=5, \
                    help = 'the amount of iterations in wich the algorithm has not removed a rule to stop', )
parser.add_argument('-f', '--filepath', \
                    type=str, default="examples/use_case_dummy.json", \
                    help="filepath of the json file that can be used as the examples")
parser.add_argument('-o', '--optimised', \
                    action='store_true', help="use a little bit more optimisation")

args = parser.parse_args()

CA = ConsAcq(iters_examples = args.iterations, \
            verbose = args.verbose, \
            optimisation = args.optimised)

if '\\' in args.filepath:
    filepath = args.filepath.split('\\')
else:
    filepath = args.filepath.split('/')

CA.do_all(filepath, 0)
