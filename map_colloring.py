import os
import sys
from pprint import pprint

from typing import Dict, List, Tuple

from FOLACQ import FOLACQ as FOLAcq

if __name__ == "__main__":
    rel_func_list: List[str] = ['=', '~=']
    inverse_rel_func: Dict[str, str] = {'=': '~=', '~=': '='}

    rel_pred_list: List[str] = ['&', '|']

    type_dict: Dict[str, List[str]] = {"Color": ['Red', 'Green', 'Yellow', 'Blue', 'Purple'],
                                       "Country": ["Albania", "Austria", "Belarus", "Belgium", "Bosnia_and_Herzegovina", "Bulgaria", "Croatia", "Czechia", "Denmark", "Estonia", "Finland", "France", "Germany", "Greece", "Hungary", "Iceland", "Ireland", "Italy", "Kosovo", "Latvia", "Lithuania", "Luxembourg", "Netherlands", "Norway", "Macedonia", "Montenegro", "Moldova", "Poland", "Portugal", "Romania", "Russia", "Serbia", "Slovakia", "Slovenia", "Spain", "Sweden", "Switzerland", "Ukraine", "UK"]}

    function_list: Dict[str, Tuple[List[str], str]] = {"colorOf": (["Country"], "Color"),
                                                       "bordering": (["Country", "Country"], "Bool"),
                                                       "Benelux": (["Country"], "Bool"),}

    enumerations = {
        "Benelux": ["Belgium", "Netherlands", "Luxembourg"]
    }

    output_symbols = ["colorOf", "bordering"]

    CA = FOLAcq(type_dict=type_dict, function_dict=function_list, rel_func_list=rel_func_list, inverse_rel_func=inverse_rel_func, rel_pred_list=rel_pred_list, output_symbols=output_symbols, enumerations=enumerations)
    CA.get_constraints("map_colloring.json", one_pred=True, value=True, opt_int_right=False)
