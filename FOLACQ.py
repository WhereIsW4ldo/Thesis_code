import os

from typing import List, Dict, Tuple

from building_blocks.formula import Formule

from building_blocks.axiom import Axiom
from building_blocks.function import Function
from building_blocks.formula import Formule
from building_blocks.implication import Implicatie
from building_blocks.quantificatie import Quantificatie

from logger import Logger

import json
import random
import time

import sys
sys.path.insert(1, os.path.join('..', 'IDP-Z3'))
from idp_engine.API import KB

class FOLACQ:
    def __init__(self, type_dict: Dict[str, List["str | int"]], function_dict: Dict[str, Tuple[List[str], str]], rel_func_list: List[str], inverse_rel_func: Dict[str, str], rel_pred_list: List[str], output_symbols: List[str], enumerations: Dict[str, List[str]]):
        self.type_dict = type_dict
        self.function_dict = function_dict
        self.rel_func_list = rel_func_list
        self.inverse_func_list = inverse_rel_func
        self.rel_pred_list = rel_pred_list
        self.output_symbols = output_symbols
        self.enumerations = enumerations
        self.logger = Logger()

    def get_constraints(self, path: str, value: bool = False, one_pred: bool = False, two_pred: bool = False, opt_int_right: bool = False):
        time_start = time.time()
        self.value = value
        self.one_pred = one_pred
        self.two_pred = two_pred
        self.opt_int_right = opt_int_right
        self.load_data(path=path)
        self.function_dict_to_Function(self.function_dict)
        self.get_formulae()
        self.get_implications()
        self.create_idp()
        self.add_types()
        self.add_functions()
        self.add_axioms()
        self.logger.write_amount(path, "start constraints: " + str(len(self.dict_axioms)))
        amount_constraints = []
        for i in range(len(self.examples_dict)):
            if len(self.dict_axioms) not in amount_constraints:
                amount_constraints.append(len(self.dict_axioms))
                self.logger.write_output(str(len(self.dict_axioms)) + path, self.idp_end)
            self.enumerate(i)
            _, bad_props = self.propagate()
            self.rem_unsat_axioms(bad_props=bad_props)
            time_passed = time.time() - time_start
            time_start = time.time()
            self.logger.write_amount(path, str("time: " + str(time_passed) + " constraints: " + str(len(self.dict_axioms))))
        self.logger.write_output(path, self.idp_end)

    def function_dict_to_Function(self, function_dict: Dict[str, Tuple[List[str], str]]):
        self.list_function: List[Function] = []
        for key, value in function_dict.items():
            name = key
            input = value[0]
            output = value[1]
            axiom_list = []
            for inp in input:
                axiom_list.append(Axiom(inp))
            self.list_function.append(Function(name, axiom_list, output))

    def pred_list_to_Function(self, function_dict: Dict[str, List[str]]):
        self.list_pred = []
        for key, value in function_dict.items():
            name = key
            input = value
            axiom_list = []
            for inp in input:
                axiom_list.append(Axiom(inp))
            self.list_pred.append(Function(name, axiom_list, "Bool"))

    def get_formulae(self):
        self.list_formulae_left: List[Formule] = []
        self.list_formulae_right: List[Formule] = []

        self.formula_fun_rel_fun()
        
        self.formula_fun_rel_out()
        

        if self.value:
            self.formula_fun_rel_val()
        
        if self.one_pred:
            self.formula_pred()
            
        if self.two_pred:
            self.formula_pred_rel_pred()

    def formula_fun_rel_fun(self):
        for rel in self.rel_func_list:
            for func1 in self.list_function:
                for func2 in self.list_function:
                    if Formule(func2, rel, func1) in self.list_formulae_left:
                        continue
                    if func1.output_type != func2.output_type:
                        continue
                    if func1.output_type == "Bool":
                        continue
                    self.list_formulae_left.append(Formule(func1, rel, func2))
                    self.list_formulae_right.append(Formule(func1, rel, func2))

    def formula_fun_rel_out(self):
        for rel in self.rel_func_list:
            for func in self.list_function:
                if func.output_type == "Bool":
                    continue
                self.list_formulae_right.append(Formule(func, rel, func.output_type))

    def formula_fun_rel_val(self):
        for rel in self.rel_func_list:
            for func1 in self.list_function:
                if func1.output_type == "Bool":
                    continue
                if func1.symbol not in self.function_dict:
                    continue
                if func1.symbol in self.enumerations:
                    continue
                ot = self.function_dict[func1.symbol][1]
                for value in self.type_dict[ot]:
                    self.list_formulae_right.append(
                        Formule(func1, rel, Axiom(value)))

    def formula_pred(self):
        for pred1 in filter(lambda function: function.output_type == "Bool", self.list_function):
            self.list_formulae_left.append(pred1)
            self.list_formulae_left.append(pred1.negate())

            if self.opt_int_right:
                if pred1.symbol not in self.output_symbols and pred1.symbol not in self.enumerations:
                    self.list_formulae_right.append(pred1)
                    self.list_formulae_right.append(pred1.negate())
            
            else:
                self.list_formulae_right.append(pred1)
                self.list_formulae_right.append(pred1.negate())
    
    def formula_pred_rel_pred(self):
        for pred1 in filter(lambda function: function.output_type == "Bool", self.list_function):
            for pred2 in filter(lambda function: function.output_type == "Bool", self.list_function):
                for rel in self.rel_pred_list:
                    self.list_formulae_left.append(Formule(pred1.negate(), rel, pred2.negate()))
                    self.list_formulae_left.append(Formule(pred1, rel, pred2))


    def get_implications(self):
        self.list_implications = []
        for formula1 in self.list_formulae_left:
            for formula2 in self.list_formulae_right:
                if str(formula1) == str(formula2):
                    continue
                if (type(formula1) is not Function) and (type(formula2) is not Function) and (formula1.func0 == formula2.func0) and (formula1.func1 == formula2.func1) and (self.inverse_func_list[formula1.comp] == formula2.comp):
                    continue
                if str(Implicatie(formula1, formula2)) in [str(x) for x in self.list_implications]:
                    continue
                self.list_implications.append(Implicatie(formula1, formula2))

    def load_data(self, path: str):
        with open(os.path.join("examples", path)) as file_data:
            data = json.load(file_data)

        self.examples_dict: List[Dict[str, Dict[str, "str | int"]]] = []
        for solution in data["positive"]:
            self.examples_dict.append(solution)
        
        random.shuffle(self.examples_dict)

        return self.examples_dict

    def create_idp(self):
        self.idp = KB()
        self.idp_end = KB()

    def add_types(self):
        for type, enums in self.type_dict.items():
            self.idp.type(type, enums)
            self.idp_end.type(type, enums)

    def add_functions(self):
        for symbol, params in self.function_dict.items():
            if params[1] == "Bool":
                if symbol in self.output_symbols:
                    self.idp.predicate(symbol, params[0])
                    self.idp_end.predicate(symbol, params[0])
                    continue
                self.idp.predicate(
                    symbol, params[0], enumeration=self.enumerations[symbol])
                self.idp_end.predicate(
                    symbol, params[0], enumeration=self.enumerations[symbol])
                continue
            self.idp.function(symbol, params[0], params[1])
            self.idp_end.function(symbol, params[0], params[1])

    def add_axioms(self):
        self.dict_axioms = {}
        self.qua = Quantificatie([x for x in self.type_dict.keys()])
        for i, impl in enumerate(self.list_implications):
            self.idp.proposition(f"p{i}")
            axiom = "p" + str(i) + "() <=> " + "(" + \
                self.qua.fill_in(impl) + ")"
            self.dict_axioms[f"p{i}"] = self.idp.axiom(axiom)
            self.dict_axioms[f"p{i}"] = self.idp_end.axiom(
                str(self.qua.fill_in(impl)))

    def enumerate(self, index: int):
        for symbol in self.output_symbols:
            enum = self.examples_dict[index][symbol]
            try:
                if self.function_dict[symbol][1] != "Bool" and len(self.function_dict[symbol][0]) > 1:
                    s = self.examples_dict[index][symbol].keys()
                    data = []
                    for k in s:
                        data.append(self.examples_dict[index][symbol][k])
                    s_temp: List[Tuple[str, str]] = []
                    for key in s:
                        s_temp.append(tuple((key.split(','))))
                    s = s_temp
                    enum = {}
                    for i in range(len(s)):
                        enum[s[i]] = data[i]
            except:
                enum = [str(x) for x in enum]
            self.idp.set_enumeration(symbol, enum)

    def propagate(self):
        print(f"Start propagating with {len(self.dict_axioms)}")
        bad_props, good_props = self.idp.propagate() # type: ignore
        return bad_props, good_props

    def rem_unsat_axioms(self, bad_props):
        for prop, value in bad_props.items():
            if value:
                axiom_id = self.dict_axioms.pop(prop)
                self.idp.rem_axiom(axiom_id)
                self.idp_end.rem_axiom(axiom_id)
        print(f"Remaining {len(self.dict_axioms)}")
