# Welcome to my gitlab repo for my thesis.

# Dependancies:

## Structure:
How should your folder structure look like?

- IDP-Z3
- Thesis_code

Both of these folders should be next to each other.

## How to install:

This project was created using Python3.10.
This means that it is possible that for example Python3.8 has some undiscovered bugs present.

Install python requirements:
```
pip3 install -r requirements.txt
```

Install IDP-Z3 repo (in same directory as this repo):
```
git clone https://gitlab.com/WhereIsW4ldo/IDP-Z3.git
```

This repo has to be set to the `Pyidpz3_imp' branch.
You can do this by typing (if you are in the folder of the IDP-Z3 project):
```
git checkout Pyidpz3_imp
```

## How to use:

1. Create python file
2. Import FOLACQ from FOLACQ.py
3. FOLACQ's constructor takes in 7 parameters:
   1. a dictionary of types with their domain
   2. a list dictionary of functions with their input and output types
   3. a list of relations (for functions)
   4. an inverse list of relations (for functions)
   5. a list of relations (for predicates)
   6. a list of symbols that are present in the JSON file
   7. a dictionary of symbols with their interpretation that is valid for each example in the JSON file
4. To run our algorithm: execute get_constraints with some parameters:
   1. the path to the JSON file (in the examples folder)
   2. value: enable generation of constraints of type: `func(x) = 1`
   3. one_pred: enable generation of constraints of type: `pred(x)`
   4. two_pred: enable generation of constraints of type: `pred1(x) & pred2(x)`
   5. opt_int_right: enable optimalisation that interpreted predicates are only allowed on the left side
5. Run python file
6. All examples will be iterated and then the python script stops
7. output file will be written to: `results/name_of_input_file.idp`

Examples for python files can be found in the main directory (`greater_than_sudoku.py`, `map_colloring.py`, ...).

## Examples:

The JSON file should be in the directory `examples`.

The structure of the JSON file should be the following:
```
{
   positive:
   [
      {
         "function1": 
         {
            "value1,value2": "value3"
         },
         "function2":
         {
            "value1": "value3"
         },
         "predicate1":
         [
            "value1", "value2", "value3"
         ],
         "predicate2":
         [
            ["value1", "value2"], ["value2", "value3"]
         ]
      },
      {
         ...
      }
   ]
}
```
This structure consists of a few important parts:
1. Every example should be present in the list of `positive` (examples).
2. Functions with one input parameter are formatted as `function2`
3. Functions with multiple input parameters are formatted as `function1`
4. Predicates with on input parameter are formatted as `predicate1`
5. Predicates with multiple input parameters are formatted as `predicate2`

For more examples over how to format the examples, see directory `examples` that were used to generate statistics for my thesis.