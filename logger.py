import os

class Logger:
    def write_output(self, path: str, idp_system):
        with open(os.path.join("results", os.path.split(path)[-1].split('.')[0] + '.idp'), "w") as file:
            file.write(idp_system.generate_voc())
            file.write(idp_system.generate_structure())
            file.write(idp_system.generate_theory())
    
    def write_amount(self, path:str, amount: str):
        with open(os.path.join("results", os.path.split(path)[-1].split('.')[0] + '.txt'), "a") as file:
            file.write(amount + "\n")