from typing import List
from building_blocks.axiom import Axiom
from building_blocks.formula import Formule
from building_blocks.function import Function

# Implicatie = Formule -> Formule
class Implicatie:
    def __init__(self, form0: Formule, form1: Formule) -> None:
        self.form0 = form0
        self.form1 = form1
        return
    
    def set_form0(self, vars_func0: List[Axiom], vars_func1: List[Axiom]) -> None:
        self.form0.set_func0(vars_func0)
        self.form0.set_func1(vars_func1)

    def set_form1(self, vars_func0: List[Axiom], vars_func1: List[Axiom]) -> None:
        self.form1.set_func0(vars_func0)
        self.form1.set_func1(vars_func1)
    
    def get_used_funcs(self) -> List[List[Function]]:
        return [self.form0.get_used_funcs(), self.form1.get_used_funcs()]

    def __str__(self) -> str:
        s = f"{self.form0} => {self.form1}"
        return s
    
    def setID(self, id: int) -> None:
        self.id = id