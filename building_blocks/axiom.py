
# Axiom = Constant / Variabele
class Axiom:
    def __init__(self, symbol: "str | int") -> None:
        self.symbol = symbol
        return

    def get_symbol(self) -> str:
        return ""

    def get_str(self) -> str:
        s = f"Axiom with symbol: {self.symbol}."
        return s
    
    def set_symbols(self, var: str) -> None:
        self.symbol = var[0]

    def __str__(self) -> "str | int":
        return self.symbol