from typing import List
from building_blocks.function import Function
from building_blocks.axiom import Axiom


# Formule = Function + Comp + Function
class Formule(Axiom):
    def __init__(self, func0: Axiom, rel: str, func1: Axiom) -> None:
        self.func0 = func0
        self.comp = rel
        self.func1 = func1
        return

    def get_used_funcs(self) -> list:
        temp = [self.func0.get_symbol(), self.func1.get_symbol()]
        return list(filter(lambda item: item is not None, temp))

    def __str__(self) -> str:
        s = f"{self.func0} {self.comp} {self.func1}"
        return s