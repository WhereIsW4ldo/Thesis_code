from typing import List
from building_blocks.axiom import Axiom


# Function = F(Axiom)
class Function(Axiom):
    def __init__(self, symbol: str, input_axioms: List[Axiom], output_type: str) -> None:
        self.input_axioms = input_axioms
        self.symbol = symbol
        self.output_type = output_type
        return
    
    def set_symbols(self, var: List[Axiom]) -> None:
        self.input_axioms = var
    
    def get_symbol(self) -> str:
        return self.symbol
    
    def get_input_len(self) -> int:
        return len(self.input_axioms)
    
    def negate(self):
        return Function("~" + self.symbol, self.input_axioms, self.output_type)

    def __str__(self) -> str:
        if self.symbol == "":
            return f"{self.input_axioms[0]}"
        s = f"{self.symbol}("
        for axiom in self.input_axioms[:-1]:
            s += str(axiom) + ", "
        s += str(self.input_axioms[-1]) if len(self.input_axioms) > 0 else ""
        return s + ")"