from typing import List, Dict, Tuple
from building_blocks.implication import Implicatie
import random

"""
voc = {"ValueOf": {"in": ["Row", "Column"], "out": ["CelValue"]},
               "SameRow": {"in": ["Row", "Row"], "out": ["Bool"]}}
        used_sym = ["ValueOf", "SameRow"]
"""
# Quantificatie adhv gebruik van variabelen in formule
class Quantificatie:
    def __init__(self, types):
        self.types = types
        self.type_dict = {}
        for type in self.types:
            if type[0].upper() not in self.type_dict.values():
                self.type_dict[type] = type[0].upper()
            else:
                afk = type[0].upper()
                while afk in self.type_dict.values():
                    afk = random.choice("ABCDEFGHIJKLMNOPQRSTUVWXYZ")
                self.type_dict[type] = afk

    def fill_in(self, implication: Implicatie) -> str:
        formula0 = implication.form0
        formula1 = implication.form1

        used_params = {}
        form_list = [str(formula0), str(formula1)]

        for type in self.type_dict.keys():
            for k in range(len(form_list)):
                for i in range(form_list[k].count(type)):
                    if type not in used_params.keys():
                        used_params[type] = [self.type_dict[type] + str(i)]
                    elif self.type_dict[type] + str(i) not in used_params[type]:
                        used_params[type].append(self.type_dict[type] + str(i))
                    form_list[k] = form_list[k].replace(type, self.type_dict[type] + str(i), 1)

        temp = ""

        exclusive = ""

        for key, value in used_params.items():
            temp += "!"
            temp += ", ".join(value) + " in " + key + ": "

        return temp + exclusive + " => ".join(form_list)
    

if __name__ == "__main__":
    print("executing ConsAcqFO.py main")