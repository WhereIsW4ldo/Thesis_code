import os
import sys
from pprint import pprint

from typing import Dict, List, Tuple

from FOLACQ import FOLACQ as FOLAcq


if __name__ == "__main__":
    rel_func_list: List[str] = ['=', '~=']
    inverse_rel_func: Dict[str, str] = {'=': '~=', '~=': '=', '<': '>', '>': '<'}

    rel_pred_list: List[str] = ['&']

    type_dict: Dict[str, List[str]] = {"Day": ["mon", "tue", "wed"],
                                       "Slot": ["am", "pm"],
                                       "Person": ["Frank", "Marcel", "Anneke", "Dirk", "Annabel"]}

    function_list: Dict[str, Tuple[List[str], str]] = {"Must": (["Person", "Day", "Slot"], "Bool"),
                                                       "MustBackup": (["Person", "Day", "Slot"], "Bool"),
                                                       "Cant": (["Person", "Day", "Slot"], "Bool"),
                                                       "OnlyBackup": (["Person"], "Bool"),
                                                       "Planning": (["Day", "Slot"], "Person"),
                                                       "Backup": (["Day", "Slot"], "Person")}

    enumerations = {
    }

    output_symbols = ["Must", "MustBackup", "Cant", "OnlyBackup", "Planning", "Backup"]

    CA = FOLAcq(type_dict, function_list, rel_func_list, inverse_rel_func, rel_pred_list, output_symbols, enumerations)
    CA.get_constraints("planning.json", one_pred=True, opt_int_right=True)
