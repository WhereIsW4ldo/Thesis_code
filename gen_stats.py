import matplotlib.pyplot as plt
import re
import numpy as np
import sys

try:
    name = sys.argv[1]
except:
    print("Please provide a name of the file to read from.")
    exit(1)

with open(f'results/{name}.txt', 'r') as file:
    time = []
    constraints = []
    for line in file:
        input = re.findall(r'time: (\d*\.\d*) constraints: (\d*)', line)
        if input == []:
            constraints.append(re.findall(r'start constraints: (\d*)', line)[0])
        else:
            time.append(float(input[0][0]))
            constraints.append(int(input[0][1]))

plt.plot(constraints, label = 'Constraints')
plt.gca().invert_yaxis()

time = np.array(time)

plt.plot(time, label = 'Time')
plt.xlabel('Examples iterated')
plt.gca().invert_yaxis()
plt.show()